import { ArbeiterDto } from "./ArbeiterDto";

export interface AuftragDto {
    nummer: number,
    bezeichnung: string,
    dauer: number,
    stunden: number
}