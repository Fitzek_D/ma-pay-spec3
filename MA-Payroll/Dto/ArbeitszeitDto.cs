﻿using System;
namespace MA_Payroll.Dto
{
    public class ArbeitszeitDto
    {
        public int ArbeiterId { get; set; }
        public int StundenToAdd { get; set; }
    }
}
