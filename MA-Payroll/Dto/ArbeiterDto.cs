﻿using System;
namespace MA_Payroll.Dto
{
    public class ArbeiterDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Stunden { get; set; }
        public int Lohn { get; set; }
    }
}
